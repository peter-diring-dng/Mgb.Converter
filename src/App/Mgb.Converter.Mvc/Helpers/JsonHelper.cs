﻿using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Mgb.Converter.Mvc.Helpers
{
    public class JsonHelper
    {
        public static T GetObject<T>(string jsonData)
        {
            var resolver = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            //TODO: Fix this in the client, for now: Clean up json
            //var json = Regex.Replace(jsonData, "(?<=^[^\"]*)\"|\"(?=[^\"]*$)", "").Replace("\\", "");

            return JsonConvert.DeserializeObject<T>(jsonData, resolver);
        }

        public static string GetJson(object dto)
        {
            var resolver = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            var json = JsonConvert.SerializeObject(dto, Formatting.Indented, resolver);
            return json;
        }

    }
}
