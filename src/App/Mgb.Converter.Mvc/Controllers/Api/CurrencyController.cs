﻿using System;
using System.Linq;
using Mgb.Converter.Domain.Models;
using Mgb.Converter.Interfaces.Services;
using Mgb.Converter.Mvc.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace Mgb.Converter.Mvc.Controllers.Api
{
    [Route("api/[controller]")]
    public class CurrencyController : Controller
    {
        private readonly ICurrencyService _currencyService;

        public CurrencyController(ICurrencyService currencyService)
        {
            if (currencyService == null) throw new ArgumentNullException(nameof(currencyService));
            _currencyService = currencyService;
        }

        [HttpPost, Route("[action]")]
        public IActionResult GetCurrencyNames()
        {
            var currencyNames = _currencyService.GetCurrencies().Select(o=>new {Id = o.Id,Name = o.Name});
            var json = JsonHelper.GetJson(currencyNames);
            return Content(json, "application/json");
        }

        [HttpPost, Route("[action]/{jsonData}")]
        public IActionResult GetCrossRate(string jsonData)
        {
            var crossRateSelection = JsonHelper.GetObject<CrossRateSelection>(jsonData);
            var rate = _currencyService.GetCrossExchangeRate(crossRateSelection);
            var json = JsonHelper.GetJson(rate);
            return Content(json, "application/json");
        }

        [HttpPost, Route("[action]/{jsonData}")]
        public IActionResult GetCrossRates(string jsonData)
        {
            var crossRateSelections = JsonHelper.GetObject<CrossRateSelection[]>(jsonData);
            var rate = _currencyService.GetCrossExchangeRates(crossRateSelections);
            var json = JsonHelper.GetJson(rate);
            return Content(json, "application/json");
        }

        [HttpPost, Route("[action]/{jsonData}")]
        public void Set(string jsonData)
        {
            throw new NotImplementedException();
        }
    }

   
}
