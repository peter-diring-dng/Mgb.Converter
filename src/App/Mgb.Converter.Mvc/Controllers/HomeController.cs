﻿using Microsoft.AspNetCore.Mvc;

namespace Mgb.Converter.Mvc.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

    }
}
