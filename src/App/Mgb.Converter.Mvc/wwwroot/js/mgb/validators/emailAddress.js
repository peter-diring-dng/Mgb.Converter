var mgb = mgb || {};
mgb.validators = mgb.validators || {};

mgb.validators.emailAddress = (function () {
    return {
        isValid: isValid
    };

    function isValid(val) {
        if (!val) return false;
        val = val.trim();
        if (val.length < 5) return false;
        var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,10}(?:\.[a-z]{2})?)$/i;
        if (filter.test(val)) return true;
        return false;
    }

})();
