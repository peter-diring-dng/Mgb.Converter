var mgb = mgb || {};
mgb.validators = mgb.validators || {};

mgb.validators.zipCode = (function () {
    return {
        isValid: isValid,
        getFormat: getFormat
    };

    function isValid(zipCodeValue) {
        if (zipCodeValue === "123") return true;
        else if (zipCodeValue === "234") return true;
        return false;
    }

    function getFormat() {
        return "123 45";
    }
})();
