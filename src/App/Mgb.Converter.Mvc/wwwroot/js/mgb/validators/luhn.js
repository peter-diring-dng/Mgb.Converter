var mgb = mgb || {};
mgb.validators = mgb.validators || {};

mgb.validators.luhn = (function () {
    return {
        isValid: isValid
    };

    function isValid(val) {
        if (!val)  return false; 
        if (val.length < 4)  return false; 
        var s = 0, m = 1, d, c = val.replace(/[^\d]/g, ""), t = c.length;
        for (var i = t - 1; i >= 0; i--) {
            d = c.charAt(i) * m;
            s += d > 9 ? d - 9 : d;
            m = 3 - m;
        }
        return (s % 10 === 0);
    }

})();
