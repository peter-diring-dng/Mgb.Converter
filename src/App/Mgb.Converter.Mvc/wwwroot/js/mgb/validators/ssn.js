var mgb = mgb || {};
mgb.validators = mgb.validators || {};

mgb.validators.ssn = (function () {

    return {
        isValid: isValid,
        getFormat: getFormat
    };

    function isValid(val) {
        var match = val.match(/\d{8}-\d{4}/);

        if (!match) return false;
        //Svensk medborgare
        if (mgb.validators.date.isValid(val.substr(0, 4) + "-" + val.substr(4, 2) + "-" + val.substr(6, 2))) {
            if (mgb.validators.luhn.isValid(val.substr(2, 12))) return true;
        }

        return false;
    }

    function getFormat() {
        return "yyyymmdd-xxxx";
    }

})();

