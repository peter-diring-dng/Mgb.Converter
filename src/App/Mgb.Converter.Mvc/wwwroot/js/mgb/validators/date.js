var mgb = mgb || {};
mgb.validators = mgb.validators || {};

mgb.validators.date = (function () {
    return {
        isValid: isValid
    };

    function isValid(val) {
        //Handles 1900-01-01 to 2099-12-31	in format YYYY-MM-DD in string, can also takes a date value 
        if (val instanceof Date) {
            val = (1900 + val.getYear()) + "-" + ("0" + (val.getMonth() + 1)).substr(0, 2) + "-" + ("0" + val.getDate()).substr(0, 2);
        }
        var pat = new RegExp("^[1][9][0-9]{2}-[0-1][0-9]-[0-3][0-9]|^[2][0][0-9]{2}-[0-1][0-9]-[0-3][0-9]");
        if (!pat.test(val)) { return false; }
        var tmpStr = val.replace(/-/g, "");
        var year = parseInt(tmpStr.substr(0, 4), 10);
        var month = parseInt(tmpStr.substr(4, 2), 10);
        var day = parseInt(tmpStr.substr(6, 2), 10);
        if (month < 1 || month > 12) { return false; }
        if (day < 1 || day > 31) { return false; }
        if (day > 30 && (month === 4 || month === 6 || month === 9 || month === 11)) { return false; }
        if (day > 29 && month === 2) return false;
        if (day === 29 && month === 2 && !(((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0))) { return false }
        return (true);
    }
})();
