﻿var mgb = mgb || {};
mgb.vm = mgb.vm || {};

//ViewModel for knockout
mgb.vm.ApplicationVm = function (elementId) {
    var self = this;
    var vmType = "mgb.vm.ApplicationVm";
    if (!elementId) throw new Error(vmType + ": elementId is null");

    //Private variables
    var isBound = false;
    var childViewModels = [];

    //Public methods
    self.getVmType = function () { return vmType; };

    self.bind = function () {
        if (isBound) throw new Error(vmType + ": Viewmodel already bound!");
        if (!document.getElementById(elementId)) throw new Error(vmType + ": elementId not found");
        //ko.applyBindings(self, document.getElementById(elementId));
        isBound = true;
    };

    self.addChildViewModel = function (type, viewModel) {
        childViewModels[type] = viewModel;
    };

    self.loading = function(value) {

    };

};
