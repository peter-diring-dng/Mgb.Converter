var mgb = mgb || {};
mgb.vm = mgb.vm || {};

mgb.vm.ConverterCalcVm = function (elementId, parentVm, dataContext, initFunction) {
    var self = this;
    var vmType = "mgb.vm.ConverterCalcVm";
    if (!elementId) throw new Error(vmType + ": elementId is null");
    if (!parentVm) throw new Error(vmType + ": parentVm is null");
    if (!dataContext) throw new Error(vmType + ": dataContext is null");
    //Private variables
    var isBound = false;
    var isVisible = false;
    var viewElement;
    var vmElement;


    //Ko ViewModelData
    self.amount = ko.observable(null);
    self.fromCurrencyId = ko.observable("");
    self.toCurrencyId = ko.observable("");
    self.sumCurrencyId = ko.observable("");
    self.rateDate = ko.observable("");
    self.currencyNames = ko.observableArray();
    self.tableRows = ko.observableArray();

    //Ko ViewModelState
    self.isDirty = ko.observable(false);
    function setDirty(val) { self.isDirty((!(!val)) ? true : false); }

    //Ko Computes
    self.totalAmount = ko.pureComputed(function () {
        var total = 0;
        //TODO: Loop through the tableRows and lookup the crossrates by the totalAmount currency ....
        if (self.sumCurrencyId() == null || self.sumCurrencyId().length < 0) return total;
        for (var r = 0; r < self.tableRows().length; ++r) {
            total += (self.tableRows()[r].fromAmount * self.tableRows()[r].crossRate);
        }
        return total;
    });

    self.isValid = ko.pureComputed(function () {
        if (self.amount() == null) { console.log("1"); return false; }
        if (self.amount() < 1) { console.log("2"); return false; }
        if (self.fromCurrencyId() == null) { console.log("3"); return false; }
        if (self.toCurrencyId() == null) { console.log("4"); return false; }
        if (self.rateDate() == null) { console.log("5"); return false; }
        if (self.fromCurrencyId().length < 3) { console.log("6"); return false; }
        if (self.toCurrencyId().length < 3) { console.log("7"); return false; }
        if (Date.parse(self.rateDate()) > Date.now()) { console.log("8"); return false; }
        return true;
    }, this);

    //Ko ViewModelActions
    self.addCalc = function () { addCalc(); };
    self.delRow = function (row) {
        self.tableRows.remove(row);
    };
    self.addRow = function (obj) {
        self.tableRows.push(obj);
    };

    //Public Methods
    self.getVisibleState = function () { return isVisible; };

    self.getVmType = function () { return vmType; };

    self.getElementId = function () { return elementId; }

    self.getParentVm = function () { return parentVm; };

    self.bind = function () {
        if (isBound) throw new Error(vmType + ": Viewmodel already bound!");
        viewElement = $("#" + elementId)[0];
        if (!viewElement) throw new Error(vmType + ": elementId not found : " + elementId);
        vmElement = $("#" + elementId + " .mainView")[0];
        ko.applyBindings(self, vmElement);
        addViewModelToParentVm();
        loadCurrencyNames();
        isBound = true;
    };

    //Private functions
    function addCalc() {
        if (self.fromCurrencyId().length < 3) throw new Error(vmType + " : addCalc() : invalid value: fromCurrencyName: " + self.fromCurrencyId());
        if (self.toCurrencyId().length < 3) throw new Error(vmType + " : addCalc() : invalid value: toCurrencyName: " + self.toCurrencyId());
        if (self.fromCurrencyId() === self.toCurrencyId()) throw new Error(vmType + " : addCalc() :  fromCurrencyName is same as toCurrencyName: " + self.toCurrencyId());
        if (Date.parse(self.rateDate()) > Date.now()) throw new Error(vmType + " : addCalc() :  date is in the future: " + self.rateDate());
        var crossRateSelections = [
            { "rateDate": self.rateDate(), "fromCurrencyName": self.fromCurrencyId(), "toCurrencyName": self.toCurrencyId() },
            { "rateDate": self.rateDate(), "fromCurrencyName": self.fromCurrencyId(), "toCurrencyName": self.toCurrencyId() }
        ];
        var crossRateSelection = { "rateDate": self.rateDate(), "fromCurrencyId": self.fromCurrencyId(), "toCurrencyId": self.toCurrencyId() };
        loadCrossRate(crossRateSelection);
    }

    function getCurrencyFromArray(currencyId) {
        var cur = ko.utils.arrayFirst(self.currencyNames(), function (item) {
            return item.id === currencyId;
        });
        return cur.name;
    }

    function loadCurrencyNames() {
        dataContext.getCurrencyNames(onLoadCurrencyNamesDone, onLoadCurrencyNamesError);
    }

    function onLoadCurrencyNamesDone(options) {
        if (!options) return null;
        if (options.length < 1) return null;
        if (self.currencyNames().length > 0) self.currencyNames.removeAll();
        for (var i = 0; i < options.length; i++) {
            self.currencyNames.push(options[i]);
        }
        //NOTE: TestData only....
        self.amount("100");
        self.fromCurrencyId("SEK");
        self.toCurrencyId("SEKUSDPMI");
        self.sumCurrencyId("SEKEURPMI");
        self.rateDate("2016-08-12");
        return null;
    }

    function onLoadCurrencyNamesError(error) {
        throw new Error(vmType + " > onLoadCurrencyNamesError : " + JSON.stringify(error));
    }

    function loadCrossRate(crossRateSelection) {
        dataContext.getCrossRate(crossRateSelection, onloadCrossRateDone, onloadCrossRateError);
    }

    function onloadCrossRateDone(crossRate) {
        if (crossRate == null) {
            alert("No crossrate was found for this combination");
            return;
        }
        //TODO: Add a new row....
        console.log(vmType + " : onloadCrossRateDone : crossRate : " + JSON.stringify(crossRate));
        var crossRateRow = {
            "rateDate": mgb.core.date.toDateStr(self.rateDate()),
            "fromAmount": self.amount(),
            "fromCurrencyName": getCurrencyFromArray(self.fromCurrencyId()),
            "toCurrencyName": getCurrencyFromArray(self.toCurrencyId()),
            "toAmount": self.amount() * crossRate.rate,
            "sumCurrencyId": self.sumCurrencyId(),
            "sumRate": crossRate.rate,
            "crossRate": crossRate.rate
        };
        self.addRow(crossRateRow);
    }


    function onloadCrossRateError(error) {
        throw new Error(vmType + " > onLoadCurrencyNamesError : " + JSON.stringify(error));
    }

    function loadCrossRates(crossRateSelections) {
        dataContext.getCrossRate(crossRateSelections, onloadCrossRatesDone, onloadCrossRatesError);
    }

    function onloadCrossRatesDone(crossRate) {
        if (crossRate == null) {
            alert("No crossrate was found for this combination");
            return;
        }
        //TODO: Add a new row....
        console.log(vmType + " : onloadCrossRateDone : crossRate : " + JSON.stringify(crossRate));
        var crossRateRow = {
            "rateDate": mgb.core.date.toDateStr(self.rateDate()),
            "fromAmount": self.amount(),
            "fromCurrencyName": getCurrencyFromArray(self.fromCurrencyId()),
            "toCurrencyName": getCurrencyFromArray(self.toCurrencyId()),
            "toAmount": self.amount() * crossRate.rate,
            "crossRate": crossRate.rate
        };
        self.addRow(crossRateRow);
    }


    function onloadCrossRatesError(error) {
        throw new Error(vmType + " > onLoadCurrencyNamesError : " + JSON.stringify(error));
    }

    function addViewModelToParentVm() {
        if (parentVm == null) throw new Error(vmType + ": parentVm is null!");
        if (parentVm.addChildViewModel == null) throw new Error(vmType + ": parentVm.addChildViewModel  is null! (" + parentVm.getVmType() + ")");
        parentVm.addChildViewModel("edit", self);
    }
};
