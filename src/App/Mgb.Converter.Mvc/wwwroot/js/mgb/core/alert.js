var mgb = mgb || {}; //mgb namespace
mgb.core = mgb.core || {}; //mgb.core namespace

mgb.core.alert = function (message, title) {
    if (!!mgb.app.alertModal) {
        mgb.app.alertModal.show(title || "Meddelande", message);
    }
    else alert(message);
};
