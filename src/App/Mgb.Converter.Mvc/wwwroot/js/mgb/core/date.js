﻿var mgb = mgb || {}; //mgb namespace
mgb.core = mgb.core || {}; //mgb.core namespace
mgb.core.date = mgb.core.date || {}; //mgb.core.date namespace

mgb.core.date.toDateStr = function (dateVal) {
    var val = new Date(dateVal);
    return (val.getFullYear() + "-" + ("0" + (val.getMonth() + 1)).slice(-2) + "-" + ("0" + val.getDate()).slice(-2));
};

