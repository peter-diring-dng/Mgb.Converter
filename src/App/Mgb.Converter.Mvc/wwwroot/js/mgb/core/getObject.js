var mgb = mgb || {};
mgb.core = mgb.core || {};

mgb.core.getObject = function (strObjName, context) {
    if (!context) context = window;
    var parts = strObjName.split(".");
    var obj = context;
    for (var i = 0, len = parts.length; i < len; ++i) {
        obj = obj[parts[i]];
    }
    return obj;
};
