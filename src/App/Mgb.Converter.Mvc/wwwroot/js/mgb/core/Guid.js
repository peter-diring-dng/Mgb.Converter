var mgb = mgb || {};
mgb.core = mgb.core || {};
mgb.core.guid = mgb.core.guid || {};

mgb.core.guid.create = function () {
    //Generates semisequentials GUID's
    //Ref: http://www.informit.com/articles/article.aspx?p=25862&seqNum=7 
    //Ref: http://sqlblog.com/blogs/alberto_ferrari/archive/2007/08/31/how-are-guids-sorted-by-sql-server.aspx 
    var ticks = new Date().valueOf();
    var val = ticks;
    var seed = ticks;
    var valStr = "";
    for (var i = 0; val > 0; i++) {
        var byte = val & 0xff;
        valStr = ("0" + byte.toString(16)).slice(-2) + valStr;
        val = (val - byte) / 256;
    }
    var uuid = "xxxxxxxx-xxxx-4xxx-yxxx".replace(/[xy]/g,
		function (c) {
		    var r = (seed + Math.random() * 16) % 16 | 0;
		    seed = Math.floor(seed / 16);
		    return (c === "x" ? r : (r & 0x3 | 0x8)).toString(16);
		}
	);
    return uuid + "-" + valStr.slice(-12);
};
