var mgb = mgb || {}; //mgb namespace
mgb.core = mgb.core || {}; //mgb.core namespace

mgb.core.cookie = mgb.core.cookie || {}; //mgb.core.cookie Functions
// set a cookie
mgb.core.cookie.set = function (cname, cvalue, exdays) {
    if (exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }
    else document.cookie = cname + "=" + cvalue;
};
// get a cookie
mgb.core.cookie.get = function(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(";");
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === " ") c = c.substring(1);
        if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
    }
    return "";
};
