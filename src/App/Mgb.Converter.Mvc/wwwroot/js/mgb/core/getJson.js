var mgb = mgb || {};
mgb.core = mgb.core || {}; //mgb Core Functions

//Get Json with ajax call...
mgb.core.getJson = function (url, onSuccess, onError, onBeforeSend, onComplete) {
    var options = {
        dataType: "json",
        contentType: "application/json",
        cache: false,
        type: "Get",
        success: function (jsonData) { onSuccess(jsonData); },
        error: function () { onError(); },
        beforeSend: function () {onBeforeSend(); console.log("beforeSend"); },
        complete: function () { onComplete(); }
    };
    var antiForgeryToken = $("#antiForgeryToken").val();
    if (antiForgeryToken) {
        options.headers = {
            'RequestVerificationToken': antiForgeryToken
        }
    }
    return $.ajax(url, options);
}
