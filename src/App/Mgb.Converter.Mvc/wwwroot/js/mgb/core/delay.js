var mgb = mgb || {}; //mgb namespace
mgb.core = mgb.core || {}; //mgb.core namespace

mgb.core.delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

////Usage: 
//$(window).resize(function () {
//    delay(function () {
//        alert('Resize...');
//        //...
//    }, 500);
//});
