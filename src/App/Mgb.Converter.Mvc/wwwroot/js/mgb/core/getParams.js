var mgb = mgb || {}; 
mgb.core = mgb.core || {};

mgb.core.getParams = function (obj) {
    //Convert obj to queryString params
    var str = [];
    for (var p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
};
