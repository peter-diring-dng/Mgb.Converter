var mgb = mgb || {};
mgb.app = mgb.app || {};

mgb.app.subscriptionInit = function () {
    //subscriptionEdit
    mgb.app.subscriptionEditVm = new mgb.vm.SubscriptionEditVm("subscriptionEditView", mgb.app.subscriptionVm, mgb.app.dataContext);
    mgb.app.subscriptionEditVm.bind();
};
