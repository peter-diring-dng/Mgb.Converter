var mgb = mgb || {};
mgb.app = mgb.app || {};

mgb.app.dataContext = (function () {
    var baseUrl = mgb.app.config.url;

    return {
        getHtmlView: getHtmlView,
        getCurrencyNames: getCurrencyNames,
        getCrossRate: getCrossRate,
        getCrossRates: getCrossRates
    };

    function getHtmlView(controller, action, params, onSuccess, onError) {
        var url = baseUrl + controller + "/" + action;
        ajaxGet(url, params, onSuccess, onError);
    }

    function getCrossRate(crossrateSelection, onSuccess, onError) {
        var url = baseUrl + "api/currency/getCrossRate/" + JSON.stringify(crossrateSelection);
        ajaxGet(url, "", onSuccess, onError);
    }

    function getCrossRates(crossrateSelections, onSuccess, onError) {
        var url = baseUrl + "api/currency/getCrossRates/" + JSON.stringify(crossrateSelections);
        ajaxGet(url, "", onSuccess, onError);
    }

    function getCurrencyNames(onSuccess, onError) {
        var url = baseUrl + "api/currency/getcurrencyNames";
        ajaxGet(url, "", onSuccess, onError);
    }

    function ajaxGet(url, params, onSuccess, onError) {
        var antiForgeryToken = $("#antiForgeryToken").val() || "invalid";
        var options = {
            cache: false,
            type: "POST",
            timeout: mgb.app.config.ajaxTimeout,
            beforeSend: onBeforeSendAjax,
            success: onSuccess,
            error: (!(!onError)) ? onError : onErrorAjax,
            complete: onCompleteAjax,
            headers: {
                'RequestVerificationToken': antiForgeryToken
            }
        };
        var prms = "?" + (!(!params) ? params + "&" : "") + "ncx=" + Math.random();
        $.ajax(url + prms, options);
    }

    function ajaxPut(url, data, onSuccess, onError) {
        //setTimeout(function () { onBeforeSendAjax(); }, 10);
        var antiForgeryToken = $("#antiForgeryToken").val() || "invalid";
        var options = {
            type: "POST",
            data: { jsonData: JSON.stringify(data) },
            timeout: mgb.app.config.ajaxTimeout,
            beforeSend: onBeforeSendAjax,
            success: onSuccess,
            error: (!(!onError)) ? onError : onErrorAjax,
            complete: onCompleteAjax,
            headers: {
                'RequestVerificationToken': antiForgeryToken
            }
        };
        $.ajax(url + "?nc=" + Math.random(), options);
    }

    function onBeforeSendAjax() {
        //Preloader show
        if (!(!mgb.app.applicationVm)) { mgb.app.applicationVm.loading(true); }
    }

    function onCompleteAjax() {
        //Preloader hide
        if (!(!mgb.app.applicationVm)) { mgb.app.applicationVm.loading(false); }
    }

    function onErrorAjax(data) {
        mgb.err = mgb.err || {};
        mgb.err.ajax = data;
        if (!(!mgb.app.applicationVm)) {
            mgb.app.applicationVm.alert({
                title: "Fel",
                content: data.statusText
            });
        }
    }

})();
