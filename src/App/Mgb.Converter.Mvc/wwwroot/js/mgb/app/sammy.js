var mgb = mgb || {};
mgb.app = mgb.app || {};

mgb.app.sammy = Sammy(function () {

    this.get("#:view", function () {
        var params = {};
        for (var key in this.params) {
            if (!this.params.hasOwnProperty(key)) continue;
            if (key === "view") continue;
            params[key] = this.params[key];
        }
        var result = mgb.app.navigationVm.navigate(this.params.view, params);
        if (!result) throw new Error("Navigation Error");
    });

    //this.get(/.*/, function () { //CatchAll  Route
    //    console.log(JSON.stringify(this.params));
    //});

    this.notFound = function () {
        console.log("notFound overridden 404");
        return "404";
    }
})
