﻿//references: Knockout, Knockout.Mapping
Storage.prototype.setMappedObject = function (key, mapping, value) {
    this.setItem(key, ko.mapping.toJSON(value, mapping));
}

Storage.prototype.setObject = function (key, value) {
    this.setItem(key, JSON.stringify(value));
}

Storage.prototype.getObject = function (key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
}

