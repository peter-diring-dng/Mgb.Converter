﻿//Knockout Observable that only load data on demand

ko.onDemandObservable = function (callback, target) {
    var value = ko.observable();  //private observable
    var result = ko.computed({
        read: function() {
            if (!result.loaded()) callback.call(target);
            return value();
        },
        write: function(newValue) {
            result.loaded(true);
            value(newValue);
        },
        deferEvaluation: true  
    });
    result.loaded = ko.observable();  
    result.refresh = function() {result.loaded(false);};
    return result;
};


