﻿//Knockout Extenders 
ko.extenders.checkDirty = function (target, fn) {
    //Adds Isdirty to Observable and sets it to false
    var initialValue = ko.observable(ko.mapping.toJSON(target));

    target.isDirty = ko.computed(function () {
        return ko.mapping.toJSON(target) !== initialValue();
    });

    target.markPristine = function () {
        initialValue(ko.mapping.toJSON(target));
    };

    target.subscribe(function () {
        if (target.isDirty) {
            fn(target.isDirty());
        }
    });

    return target;
};



ko.extenders.required = function (target, overrideMessage) {
    //add some sub-observables to our observable
    target.hasError = ko.observable();
    target.validationMessage = ko.observable();

    function validate(newValue) { //define a function to do validation
        target.hasError(newValue ? false : true);
        target.validationMessage(newValue ? "" : overrideMessage || "This field is required");
    }

    validate(target()); //initial validation

    target.subscribe(validate); //validate whenever the value changes

    return target; //return the original observable
};

