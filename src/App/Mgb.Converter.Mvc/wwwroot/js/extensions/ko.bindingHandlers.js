﻿//Knockout BindingHandlers

ko.bindingHandlers.hasFocus = {
    init: function (element, valueAccessor) {
        $(element).focus(function () {
            var value = valueAccessor();
            value(true);
        });
        $(element).blur(function () {
            var value = valueAccessor();
            value(false);
        });
    },
    update: function (element, valueAccessor) {
        var value = valueAccessor();
        if (ko.unwrap(value))
            element.focus();
        else
            element.blur();
    }
};

ko.bindingHandlers.valueWithInit = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var property = valueAccessor(), value = element.value;
        if (!ko.isWriteableObservable(viewModel[property])) viewModel[property] = ko.observable();
        viewModel[property](value);
        ko.applyBindingsToNode(element, { value: viewModel[property] }, bindingContext);
    }
};

ko.bindingHandlers.textWithInit = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var property = valueAccessor(), text = element.innerHTML;
        if (!ko.isWriteableObservable(viewModel[property])) viewModel[property] = ko.observable();
        viewModel[property](text);
        ko.applyBindingsToNode(element, { text: viewModel[property] }, bindingContext);
    }
};

ko.bindingHandlers.popover = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var options = valueAccessor();
        var defaultOptions = {};
        options = $.extend(true, {}, defaultOptions, options);
        $(element).popover(options);
    }
};

ko.bindingHandlers.datePicker = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var unwrap = ko.utils.unwrapObservable;
        var dataSource = valueAccessor();
        var binding = allBindingsAccessor();
        var options = {
            keyboardNavigation: true,
            todayHighlight: true,
            autoclose: true,
            daysOfWeekDisabled: [0, 6],
            format: "yyyy-mm-dd"
        };
        if (!(!binding)) {
            if (!(!binding.datePickerOptions)) {
                options = $.extend(options, binding.datePickerOptions);
            }
        }
        $(element).datepicker(options);
        $(element).datepicker("update", dataSource());
        $(element).on("changeDate", function (ev) {
            var observable = valueAccessor();
            if ($(element).is(":focus")) {
                // Don't update while the user is in the field...
                // Instead, handle focus loss
                $(element).one("blur", function () {
                    var dateVal = $(element).datepicker("getDate");
                    observable(dateVal);
                });
            }
            else {
                var year = ev.date.getFullYear();
                var month = ev.date.getMonth();
                var day = ev.date.getDate();
                var evDate = new Date(year, month, day, 12, 0, 0);
                //Need this otherwise one day off
                observable(evDate);
            }
        });
        //handle removing an element from the dom
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).datepicker("remove");
        });
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).datepicker("update", (!value) ? null : new Date(value));
    }
};

ko.bindingHandlers.selectPicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        if ($(element).is("select")) {
            if (ko.isObservable(valueAccessor())) {
                if ($(element).prop("multiple") && $.isArray(ko.utils.unwrapObservable(valueAccessor()))) {
                    // in the case of a multiple select where the valueAccessor() is an observableArray, call the default Knockout selectedOptions binding
                    ko.bindingHandlers.selectedOptions.init(element, valueAccessor, allBindingsAccessor);
                } else {
                    // regular select and observable so call the default value binding
                    ko.bindingHandlers.value.init(element, valueAccessor, allBindingsAccessor);
                }
            }
            $(element).addClass("selectpicker").selectpicker();
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        if ($(element).is("select")) {
            var selectPickerOptions = allBindingsAccessor().selectPickerOptions;
            if (typeof selectPickerOptions !== "undefined" && selectPickerOptions !== null) {
                var options = selectPickerOptions.optionsArray;
                var optionsText = selectPickerOptions.optionsText;
                var optionsValue = selectPickerOptions.optionsValue;
                var optionsCaption = selectPickerOptions.optionsCaption;
                var isDisabled = selectPickerOptions.disabledCondition || false;
                var resetOnDisabled = selectPickerOptions.resetOnDisabled || false;
                if (ko.utils.unwrapObservable(options).length > 0) {
                    // call the default Knockout options binding
                    ko.bindingHandlers.options.update(element, options, allBindingsAccessor);
                }
                if (isDisabled && resetOnDisabled) {
                    // the dropdown is disabled and we need to reset it to its first option
                    $(element).selectpicker("val", $(element).children("option:first").val());
                }
                $(element).prop("disabled", isDisabled);
            }
            if (ko.isObservable(valueAccessor())) {
                if ($(element).prop("multiple") && $.isArray(ko.utils.unwrapObservable(valueAccessor()))) {
                    // in the case of a multiple select where the valueAccessor() is an observableArray, call the default Knockout selectedOptions binding
                    ko.bindingHandlers.selectedOptions.update(element, valueAccessor);
                } else {
                    // call the default Knockout value binding
                    ko.bindingHandlers.value.update(element, valueAccessor);
                }
            }

            $(element).selectpicker("refresh");
        }
    }
};

ko.bindingHandlers.treeViewNode = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var observable = valueAccessor() || {};
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var observable = valueAccessor() || {};
        var unwrapped = ko.unwrap(observable);
        element.innerHTML = '<li><input type="checkbox" data-bind="value: id, checked: checked"/> ' + unwrapped + '<ul data-bind="foreach: nodes"><li data-bind="treeViewNode: name" /></ul></li>';
    }
};

ko.bindingHandlers.dataTablesForEach = {
    page: 0,
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var options = ko.unwrap(valueAccessor());
        ko.unwrap(options.data);
        if (options.dataTableOptions.paging) {
            valueAccessor().data.subscribe(function (changes) {
                var table = $(element).closest('table').DataTable();
                ko.bindingHandlers.dataTablesForEach.page = table.page();
                table.destroy();
            }, null, "arrayChange");
        }
        var nodes = Array.prototype.slice.call(element.childNodes, 0);
        ko.utils.arrayForEach(nodes, function (node) {
            if (node && node.nodeType !== 1) {
                node.parentNode.removeChild(node);
            }
        });
        return ko.bindingHandlers.foreach.init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var options = ko.unwrap(valueAccessor()),
            key = "DataTablesForEach_Initialized";
        ko.unwrap(options.data);
        var table;
        if (!options.dataTableOptions.paging) {
            table = $(element).closest("table").DataTable();
            table.destroy();
        }
        ko.bindingHandlers.foreach.update(element, valueAccessor, allBindings, viewModel, bindingContext);
        table = $(element).closest("table").DataTable(options.dataTableOptions);
        if (options.dataTableOptions.paging) {
            if (table.page.info().pages - ko.bindingHandlers.dataTablesForEach.page === 0)
                table.page(--ko.bindingHandlers.dataTablesForEach.page).draw(false);
            else
                table.page(ko.bindingHandlers.dataTablesForEach.page).draw(false);
        }
        if (!ko.utils.domData.get(element, key) && (options.data || options.length))
            ko.utils.domData.set(element, key, true);
        return { controlsDescendantBindings: true };
    }
};


ko.bindingHandlers.typeAhead = {
    init: function (element, valueAccessor, allBindings) {

        var options = valueAccessor();
        var optionsText = allBindings.get("textInput");

        $(element).blur(function () {
            options.id(getOptionId(options.source(), optionsText()));
            setTimeout(function () {
                if ($(element).val !== optionsText()) $(element).val(optionsText());
            }, 50);
        });

        function substringMatcher(optionArray) {
            return function (q, cb) {
                var matches = [];
                var substrRegex = new RegExp(q, "i");
                for (var i = 0; i < optionArray.length; i++) {
                    if (substrRegex.test(optionArray[i].text)) matches.push({
                        value: optionArray[i].text
                    });
                }
                cb(matches);
            };
        };

        function getOptionId(optionArray, textValue) {
            for (var i = 0; i < optionArray.length; i++) {
                if (optionArray[i].text === textValue) {
                    return optionArray[i].id;
                }
            }
            return null;
        };

        var $e = $(element);

        $e.typeahead({
            highlight: true,
            minLength: 2
        }, {
            source: substringMatcher(options.source())
        })
          .on("typeahead:selected", function (el, datum) {
              options.id(getOptionId(options.source(), datum.value));
              optionsText(datum.value);
          })
          .on("typeahead:autocompleted", function (el, datum) {
              options.id(getOptionId(options.source(), datum.value));
              optionsText(datum.value);
          });
    }
};


ko.bindingHandlers.file = {
    init: function (element, valueAccessor) {
        $(element).change(function () {
            var file = this.files[0];
            if (ko.isObservable(valueAccessor())) {
                valueAccessor()(file);
            }
        });
    },

    update: function (element, valueAccessor, allBindingsAccessor) {
        var file = ko.utils.unwrapObservable(valueAccessor());
        var bindings = allBindingsAccessor();

        if (bindings.imageBase64 && ko.isObservable(bindings.imageBase64)) {
            if (!file) {
                bindings.imageBase64(null);
                bindings.imageType(null);
            } else {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var result = e.target.result || {};
                    var resultParts = result.split(",");
                    if (resultParts.length === 2) {
                        bindings.imageBase64(resultParts[1]);
                        bindings.imageType(resultParts[0]);
                    }

                    if (bindings.fileObjectURL && ko.isObservable(bindings.fileObjectURL)) {
                        var oldUrl = bindings.fileObjectURL();
                        if (oldUrl) {
                            windowURL.revokeObjectURL(oldUrl);
                        }
                        bindings.fileObjectURL(file && windowURL.createObjectURL(file));
                    }
                };
                reader.readAsDataURL(file);
            }
        }
    }
};
