﻿using System;
using Mgb.Converter.Domain.Models;
using Mgb.Converter.Interfaces.Services;
using Moq;
using NUnit.Framework;

namespace Mgb.Converter.RateService.Tests
{
    [TestFixture]
    public class CurrencyServiceTests
    {
        private readonly Mock<ICurrencyService> _currencyService;

        public CurrencyServiceTests()
        {
            _currencyService = new Mock<ICurrencyService>();
        }

        [Test]
        public void CallGetCrossExchangeRateWithNullParameterShouldThrowExceptionWithMock()
        {
            //Assign
            _currencyService.Setup(cs => cs.GetCrossExchangeRate(null))
                .Throws<ArgumentNullException>();

            //Act&Assert
            Assert.Throws<ArgumentNullException>(() => _currencyService.Object.GetCrossExchangeRate(null));
        }
    }
}
