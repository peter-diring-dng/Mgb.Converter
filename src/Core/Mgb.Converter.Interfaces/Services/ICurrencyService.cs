﻿using System.Collections.Generic;
using Mgb.Converter.Domain.Models;

namespace Mgb.Converter.Interfaces.Services
{
    public interface ICurrencyService
    {
        ICollection<CurrencyType> GetCurrencies();
        CrossRate GetCrossExchangeRate(CrossRateSelection crossRateSelection);
        ICollection<CrossRate> GetCrossExchangeRates(CrossRateSelection[] crossSelections);
        ICollection<CrossRate> GetCrossExchangeRateSerie(CrossRateMultiSelection crossRateMultiSelection);
    }
}
