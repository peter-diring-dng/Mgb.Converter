﻿namespace Mgb.Converter.Domain.Models
{
    public class CurrencyType
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}