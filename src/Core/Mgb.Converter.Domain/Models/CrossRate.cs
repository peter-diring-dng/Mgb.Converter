﻿namespace Mgb.Converter.Domain.Models
{
    public class CrossRate
    {
        public CrossRateSelection CrossRateSelection { get; set; }
        public double? Rate { get; set; }
    }
}
