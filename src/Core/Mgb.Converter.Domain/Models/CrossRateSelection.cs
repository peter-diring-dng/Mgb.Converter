﻿using System;
using System.Collections.Generic;

namespace Mgb.Converter.Domain.Models
{
    public class CrossRateSelection
    {
        public DateTime RateDate { get; set; }
        public string FromCurrencyId { get; set; }
        public string ToCurrencyId { get; set; }
    }

    public class CrossRateMultiSelection
    {
        public DateTime RateDate { get; set; }
        public ICollection<CrossRatePair> CrossRatePairs { get; set; }
    }

    public class CrossRatePair
    {
        public string FromCurrencyId { get; set; }
        public string ToCurrencyId { get; set; }
    }
}