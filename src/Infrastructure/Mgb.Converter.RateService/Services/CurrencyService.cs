﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Mgb.Converter.Domain.Models;
using Mgb.Converter.Interfaces.Services;
using Mgb.Converter.RateService.SweaWebService;

namespace Mgb.Converter.RateService.Services
{
    public class CurrencyService : ICurrencyService
    {
        private readonly string _endPointAddress;

        public CurrencyService(string endPointAddress)
        {
            if (endPointAddress == null) throw new ArgumentNullException(nameof(endPointAddress));
            _endPointAddress = endPointAddress;
        }

        public ICollection<CurrencyType> GetCurrencies()
        {
            var binding = new BasicHttpsBinding();
            var endpoint = new EndpointAddress(_endPointAddress);
            var client = new SweaWebServicePortTypeClient(binding, endpoint);
            return client.getAllCrossNames(LanguageType.en)
                .Select(o => new CurrencyType
                {
                    Id = o.seriesid,
                    Name = o.seriesname,
                    Description = o.seriesdescription
                })
                .OrderBy(s => s.Name)
                .ToList();
        }

        public CrossRate GetCrossExchangeRate(CrossRateSelection crossRateSelection)
        {
            if (crossRateSelection == null) throw new ArgumentNullException(nameof(crossRateSelection));
            var binding = new BasicHttpsBinding();
            var endpoint = new EndpointAddress(_endPointAddress);

            var client = new SweaWebServicePortTypeClient(binding, endpoint);

            var crp = new CrossRequestParameters
            {
                datefrom = crossRateSelection.RateDate,
                dateto = crossRateSelection.RateDate.AddDays(1),
                aggregateMethod = AggregateMethodType.D,
                languageid = LanguageType.en,
                crossPair = new[] { new CurrencyCrossPair
                {
                    seriesid1 = crossRateSelection.FromCurrencyId,
                    seriesid2 = crossRateSelection.ToCurrencyId
                }}
            };
            var crossRateGroups = client.getCrossRates(crp).groups;
            if (crossRateGroups == null) return null;
            var crossRateGroup = crossRateGroups.FirstOrDefault();
            var crossResultSeries = crossRateGroup?.series.FirstOrDefault();
            if (crossResultSeries == null) throw new NullReferenceException(nameof(crossResultSeries));
            return crossResultSeries.resultrows
                .Select(o => new CrossRate
                {
                    CrossRateSelection = new CrossRateSelection
                    {
                        RateDate = crossRateSelection.RateDate,
                        FromCurrencyId = crossRateSelection.FromCurrencyId,
                        ToCurrencyId = crossRateSelection.ToCurrencyId
                    },
                    Rate = o.value
                })
                .FirstOrDefault();
        }

        public ICollection<CrossRate> GetCrossExchangeRates(CrossRateSelection[] crossRateSelections)
        {
            if (crossRateSelections == null) throw new ArgumentNullException(nameof(crossRateSelections));

            var crossExchangeRates = new List<CrossRate>();
            //Group/Split on RateDate since this should shrink the number of calls to the webservice
            var groupedCrossRateSelections = crossRateSelections.GroupBy(x => x.RateDate).Select(g => g.ToList()).ToList();
            foreach (var groupedCrossRateSelection in groupedCrossRateSelections)
            {
                var crossRatePairs = groupedCrossRateSelection.Select(gr => new CrossRatePair { FromCurrencyId = gr.FromCurrencyId, ToCurrencyId = gr.ToCurrencyId }).ToList();
                var rateDate = groupedCrossRateSelection.Select(o => new { o.RateDate }).FirstOrDefault();
                if (rateDate == null) throw new NullReferenceException(nameof(rateDate));
                var crossRateMultiSelection = new CrossRateMultiSelection
                {
                    RateDate = rateDate.RateDate,
                    CrossRatePairs = crossRatePairs
                };
                crossExchangeRates.AddRange(GetCrossExchangeRateSerie(crossRateMultiSelection));
            }
            return crossExchangeRates;
        }

        public ICollection<CrossRate> GetCrossExchangeRateSerie(CrossRateMultiSelection crossRateMultiSelection)
        {
            if (crossRateMultiSelection == null) throw new ArgumentNullException(nameof(crossRateMultiSelection));
            var client = new SweaWebServicePortTypeClient();

            var crp = new CrossRequestParameters
            {
                datefrom = crossRateMultiSelection.RateDate,
                dateto = crossRateMultiSelection.RateDate.AddDays(1),
                aggregateMethod = AggregateMethodType.D,
                languageid = LanguageType.en,
                crossPair = crossRateMultiSelection.CrossRatePairs.Select(o => new CurrencyCrossPair { seriesid1 = o.FromCurrencyId, seriesid2 = o.ToCurrencyId }).ToArray()
            };
            var crossRates = client.getCrossRates(crp);
            if (crossRates == null) throw new ArgumentNullException(nameof(crossRates));
            var crossRatesGroups = crossRates.groups;
            if (crossRatesGroups == null) throw new ArgumentNullException(nameof(crossRatesGroups));
            foreach (var series in crossRatesGroups.Select(crossRatesGroup => crossRatesGroup.series))
            {
                if (series == null) throw new ArgumentNullException(nameof(series));
                foreach (var serie in series)
                {
                    var resultrows = serie.resultrows;
                    if (resultrows == null) throw new ArgumentNullException(nameof(resultrows));
                    return resultrows.AsQueryable().Select(o => new CrossRate { CrossRateSelection = new CrossRateSelection { RateDate = crossRateMultiSelection.RateDate, FromCurrencyId = serie.seriesid1.Trim(), ToCurrencyId = serie.seriesid2.Trim() }, Rate = o.value }).ToList();
                }
            }
            return null;
        }
    }
}
